npm:
	npm install

build_cljs: .FORCE
	clj -M:build

clean:
	rm -rf "./build"
	rm -rf "./ir-blaster-native"

native_debug: .FORCE
	mkdir -p ./native/build
	gcc ./native/main.c -DDEBUG -lm -pthread -lrt -o ./ir-blaster-native

native:	.FORCE
	mkdir -p ./native/build
	gcc ./native/main.c -lm -lpigpio -pthread -lrt -o ./ir-blaster-native


bundle: build_cljs
	mkdir -p ./bundle
	cp ./package-lock.json ./bundle
	cp ./package.json ./bundle
	cp ./build/index.js ./bundle
	cp -r ./native ./bundle/
	cp ./scripts/Makefile_install ./bundle/Makefile
	cp ./scripts/ir-blaster.service ./bundle/
	cp ./scripts/environment ./bundle/
	tar -cvf bundle.tar.gz ./bundle 
	rm -rf ./bundle

debug: npm native_debug build_cljs 

.FORCE:
