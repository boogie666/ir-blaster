(ns cljs.user
  (:require [ir-blaster.core :as core]
            [com.stuartsierra.component :as c]))

(def ^:private state (atom (core/application)))

(defn start []
  (swap! state c/start))

(defn stop []
  (swap! state c/stop)) 
    

(defn go []
  (stop)
  (reset! state (core/application))
  (start))

