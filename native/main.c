#define OUT_PIN 22
#define FREQ 38000
#define DUTY_CYCLE 0.5

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#ifndef DEBUG
 #include "./irslinger.h"
#endif

int parseCodes(int count, char *vals[], int* results){
    for(int i = 1; i < count; i++){
	    int result = atoi(vals[i]);
	    results[i-1] = result;
    }
    return 0;
}

void irBlast(const int *codes, int len){
  #ifdef DEBUG
    printf("Sending IR codes\n");
    for(int i = 0; i < len; i++){
      printf("%d\n", codes[i]);
    }
  #else
    int result = irSlingRaw(
	OUT_PIN,
	FREQ,
	DUTY_CYCLE,
	codes,
	len
    );
  #endif
}

int main(int argc, char *argv[])
{
		

	int *codes = malloc(sizeof(int) * (argc - 1));
	int result = parseCodes(argc, argv, codes);
	if(!result){
	  irBlast(codes, argc-1);
	}else {
	  printf("Error #%d\n", result);
	}

	free(codes);
	return 0;
}
