(ns ir-blaster.command
  (:require ["child_process" :as cp]
            [com.stuartsierra.component :refer [Lifecycle]]
            [ir-blaster.log :refer [log error]]
            [clojure.core.async :as a]))

(defn- exec-command! [executable args]
  (let [result (a/promise-chan)]
    (.exec cp (str executable " " args) (fn [error stdout]
                                          (if error
                                            (a/put! result [:error error])
                                            (a/put! result [:ok (.toString stdout)]))))
    result))

(defrecord Command [config queue]
  Lifecycle
  (start [this]
    (let [queue (a/chan)]
      (a/go-loop []
        (when-let [ir-code (a/<! queue)]
          (let [[status data] (a/<! (exec-command! (:executable config) ir-code))]
            (case status
              :ok (log data)
              :error (error data)))
          (a/<! (a/timeout (:delay config)))
          (recur)))
      (assoc this :queue queue)))
  (stop [this] this))

(defn exec! [{:keys [config queue]} ir-code]
  (let [codes (repeat (:repeats config) ir-code)]
    (a/onto-chan! queue codes false))) 


(defn new-command [config] 
  (->Command config nil))
