(ns ir-blaster.core
  (:require [ir-blaster.mqtt :refer [new-mqtt]]
            [ir-blaster.command :refer [new-command]]
            [ir-blaster.remote-control :refer [new-remote]]
            [com.stuartsierra.component :as c :refer [system-map using]]
            [goog.object :as g]))

(defn env-var
  ([name]
   (env-var name nil identity))
  ([name default]
   (env-var name default identity))
  ([name default f]
   (if-let [env (g/get js/process "env" nil)]
     (if-let [val (g/get env name)]
       (f val)
       default)
     default)))

(defn config []
  {:mqtt {:port (env-var "PORT" 1883 int)
          :host (env-var "HOST" "localhost")
          :topic (env-var "MQTT_TOPIC" "home-assistant/living-room-ac/command")} 
   :ir-blast {:executable (env-var "IR_BLAST" "./ir-blaster-native")
              :delay (env-var "DELAY_BETWEEN_COMMANDS" 250 int)
              :repeats (env-var "REPEAT_COMMANDS" 1 int)}}) 

(defn application []
  (let [config (config)]
    (system-map
     :mqtt (new-mqtt (:mqtt config))
     :ir-command (new-command (:ir-blast config))
     :remote-control (-> (new-remote (-> config :mqtt :topic)) 
                         (using [:mqtt :ir-command])))))

(defn -main [& args]
  (c/start (application)))



(comment
  (cljs.user/go))
