(ns ir-blaster.log)

(defn log [& stuff]
  (apply (.-log js/console) (js->clj stuff)))

(defn info [& stuff]
  (apply (.-info js/console) (js->clj stuff)))

(defn warn [& stuff]
  (apply (.-warn js/console) (js->clj stuff)))


(defn error [& stuff]
  (apply (.-error js/console) (js->clj stuff)))
