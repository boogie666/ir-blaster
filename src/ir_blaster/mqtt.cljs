(ns ir-blaster.mqtt
  (:require [com.stuartsierra.component :refer [Lifecycle start stop]]
            [ir-blaster.log :refer [log]]
            [clojure.core.async :as a]
            ["mqtt" :as mqtt]))

(defrecord MQTT [client config]
  Lifecycle
  (start [this]
    (log "Connecting to MQTT" (:host config) (:port config))
    (assoc this :client (mqtt/connect (clj->js config))))
  (stop [this] this
    (update this :client #(some-> % .end))))

(defn new-mqtt [config]
  (->MQTT nil config))

(defn subscribe!
  ([client topic]
   (subscribe! client topic (a/chan (a/sliding-buffer 5))))
  ([{:keys [client]} topic chan]
   (log "MQTT:" "subscribed on" topic)
   (some-> client
           (.subscribe topic)
           (.on "message" (fn [t data]
                            (log "messages: got" (.toString data "utf-8"))
                            (when (= t topic)
                              (a/put! chan (.toString data "utf-8")))))) 
                   
   chan))

(defn unsubscribe! [{:keys [client]} topic]
  (some-> client
      (.unsubscribe topic)))
