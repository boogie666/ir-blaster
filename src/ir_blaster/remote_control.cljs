(ns ir-blaster.remote-control
  (:require [ir-blaster.command :refer [exec!]]
            [ir-blaster.mqtt :refer [subscribe! unsubscribe!]]
            [ir-blaster.log :refer [log]]
            [com.stuartsierra.component :refer [Lifecycle]]
            [clojure.core.async :as a]
            [clojure.string :as str]))

(defn- atob [base64]
  (.toString (.from js/Buffer base64 "base64") "binary"))

(defn- base64->hex [string]
  (let [bin (seq (atob (str/replace string #"[ \r\n]+$" "")))]
    (str/join "" (map (fn [bin]
                        (let [tmp (.charCodeAt bin 0)
                              tmp (.toString tmp 16)]
                          (if (= 1 (count tmp))
                            (str "0" tmp)
                            tmp)))
                      bin))))

(def ^:private BRDLNK_UNIT (/ 269 8192))

(defn- decode-command [string]
  (let [string (base64->hex string)
        length (js/parseInt (str (subs string 6 8)
                                 (subs string 4 6))
                            16)]
    (loop [result []
           i 8]
      (if (>= i (+ 8 (* length 2)))
        (str/join ", " result)
        (let [hex (subs string i (+ i 2))
              [i hex] (if (= "00" hex)
                        (let [hex (str (subs string (+ i 2) (+ i 4))
                                       (subs string (+ i 4) (+ i 6)))]
                          [(+ i 4) hex])
                        [i hex])]

          (recur (conj result
                       (js/Math.ceil (/ (js/parseInt hex 16) BRDLNK_UNIT)))
                 (+ i 2)))))))

(defrecord RemoteControl [topic ir-command mqtt message-chan]
  Lifecycle
  (start [this]
    (log "Starting remote control on mqtt topic" topic)
    (let [messages (subscribe! mqtt topic)]
      (a/go-loop []
        (when-let [m (a/<! messages)]
          (log "remote-control: got message" m)
          (exec! ir-command (decode-command m))
          (recur))
       (assoc this :message-chan messages))))
  (stop [this]
    (when mqtt
      (unsubscribe! mqtt topic))
    (when message-chan
      (a/close! message-chan))
    (-> this
        (assoc :message-chan nil))))

(defn new-remote [topic]
  (->RemoteControl topic nil nil nil))
